const eventsData = [{
  "name": "PUBG",
  "region": "US",
  "createdOn": "2019-12-11",
  "price": [
    { durationText: [ "3 meses","3 months"], durationAmount: "$300.00" },
    { durationText: ["6 meses","6 months"], durationAmount: "$500.00" },
    { durationText: ["1 año","1 Year"], durationAmount: "$900.00" }
  ],
  "csv": "Some CSV link for Whatsapp",
  "report": "Some report link for Whatsapp",
  "imageKey": "camp6"
},
{
  "name": "Super Jewels Quest",
  "region": "CA, FR",
  "createdOn": "2019-12-11",
  "price": [
    { durationText: ["1 mes","1 month"], durationAmount: "$200.00" },
    { durationText: ["2 mes","2 month"], durationAmount: "$350.00" },
    { durationText: ["4 mes","4 month"], durationAmount: "$450.00" }
  ],
  "csv": "Some CSV link for Super Jewels Quest",
  "report": "Some report link for Super Jewels Ques",
  "imageKey": "camp2"
},
{
  "name": "Mole Slayer",
  "region": "FR",
  "createdOn": "2019-12-11",
  "price": [
    { durationText: ["2 mes","2 month"], durationAmount: "$200.00" },
    { durationText: ["4 mes","4 month"], durationAmount: "$320.00" },
    { durationText: ["7 mes","7 month"], durationAmount: "$450.00" }
  ],
  "csv": "Some CSV link for Mole Slayer",
  "report": "Some report link for Mole Slayer",
  "imageKey": "camp7"
},
{
  "name": "Mancala Mix",
  "region": "JP",
  "createdOn": "2019-12-11",
  "price": [
    { durationText: ["1 mes","2 month"], durationAmount: "$50.00" },
    { durationText: ["3 mes","3 month"], durationAmount: "$120.00" },
    { durationText: ["5 mes","5 month"], durationAmount: "$180.00" }
  ],
  "csv": "Some CSV link for Mancala Mix",
  "report": "Some report link for Mancala Mix",
  "imageKey": "camp8"
},
{
  "name": "Summoners War",
  "region": "US",
  "createdOn": "2019-10-28",
  "price": [
    { durationText: ["1 mes","2 month"], durationAmount: "$50.00" },
    { durationText: ["3 mes","3 month"], durationAmount: "$120.00" },
    { durationText: ["5 mes","5 month"], durationAmount: "$180.00" }
  ],
  "imageKey": "camp3"
},
{
  "name": "Need for Speed No Limits",
  "region": "US",
  "createdOn": "2019-08-21",
  "price": [
    { durationText: ["1 mes","2 month"], durationAmount: "$50.00" },
    { durationText: ["3 mes","3 month"], durationAmount: "$120.00" },
    { durationText: ["5 mes","5 month"], durationAmount: "$180.00" }
  ],
  "imageKey": "camp5"
},
{
  "name": "Shadow fight 3",
  "region": "US",
  "createdOn": "2019-10-25",
  "price": [
    { durationText: ["1 mes","2 month"], durationAmount: "$50.00" },
    { durationText: ["3 mes","3 month"], durationAmount: "$120.00" },
    { durationText: ["5 mes","5 month"], durationAmount: "$180.00" }
  ],
  "imageKey": "camp4"
},
{
  "name": "Auto Chess",
  "region": "US",
  "createdOn": "2019-07-19",
  "price": [
    { durationText: ["1 mes","2 month"], durationAmount: "$50.00" },
    { durationText: ["3 mes","3 month"], durationAmount: "$120.00" },
    { durationText: ["5 mes","5 month"], durationAmount: "$180.00" }
  ],
  "imageKey": "camp1"
},

];

export default eventsData;