import React from 'react';
import campaignsConfig from "./config";
import Header from './Components/Header';
import Campaigns from "./Components/Campaigns";

import './App.css';


class App extends React.Component {
  state = {
    data: campaignsConfig,
    isMobileView: false
  }

  componentDidMount() {
    this.checkViewPort();
    window.addEventListener('resize', this.checkViewPort);
  }

  checkViewPort = () => this.setState({ isMobileView: window.matchMedia("(max-width:768px)").matches });

  onChange = (date, index) => {
    const data = [...this.state.data];
    data[index].createdOn = date
    this.setState({ data });
  }

  render() {
    const { isMobileView } = this.state;
    return (
      <div className="App">
        <Header />
        <main><Campaigns data={this.state.data} onChange={this.onChange} mode={isMobileView ? "mobile" : "desktop"} /></main>
      </div>
    );
  }
}

export default App;