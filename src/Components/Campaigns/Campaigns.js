import React, { useState } from 'react';
import './Campaigns.css';
import CampaignTable from "../CampaignTable";
import ToggleButton from "../ToggleButton";

const textConfig = {
    english: {
        mobile: {
            past: "Past",
            live: "Live",
            upcoming: "Upcoming"
        },
        desktop: {
            past: 'Past Campaigns',
            live: 'Live Campaigns',
            upcoming: 'Upcoming Campaigns'
        }
    },
    spanish: {
        mobile: {
            past: "Pasada",
            live: "En Vivo",
            upcoming: "Próxima"
        },
        desktop: {
            past: "Campaña Pasada",
            live: "Campañas En Vivo",
            upcoming: "Próxima Campaña"
        }
    }
}

const Campaigns = ({ data, onChange, mode }) => {
    const [activeTab, setActiveTab] = useState(0);
    const [lang, setLang] = useState(false);
    return <div>
        <h1>{lang ? " gestionar Campañas " : "Manage Campaigns"}</h1>
        <div className="langDiv">
            <span className="langSpan">EN</span><ToggleButton checked={lang} toggleLanguage={setLang} /><span className="langSpan">ES</span>
        </div>
        <div className="tab-wrapper">
            <span 
            className={`tab-heading ${(activeTab === 0 && 'active') || ""}`} 
            onClick={() => setActiveTab(0)}> {lang ? textConfig["spanish"][mode]["upcoming"] : textConfig["english"][mode]["upcoming"]}
            </span>
            <span 
            className={`tab-heading ${(activeTab === 1 && 'active') || ""}`} 
            onClick={() => setActiveTab(1)}>{lang ? textConfig["spanish"][mode]["live"] : textConfig["english"][mode]["live"]}
            </span>
            <span 
            className={`tab-heading ${(activeTab === 2 && 'active') || ""}`} 
            onClick={() => setActiveTab(2)}>{lang ? textConfig["spanish"][mode]["past"] : textConfig["english"][mode]["past"]}
            </span>
        </div>
        <CampaignTable lang={lang} activeTab={activeTab} data={data} onChange={onChange} mode={mode}/>
    </div>
}

export default Campaigns;