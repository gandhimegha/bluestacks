import React, { useState } from "react";
import "./CampaignTable.css";
import CampaignTableRow from '../CampaignTableRow';
import Modal from '../Modal';

const CampaignTable = ({ data, onChange, activeTab, lang, mode }) => {
    const [isModalOpen, setModalState] = useState(false);
    const [selectedCampaign, setSelectedCampaign] = useState(null);
    return <div className="table-wrapper flex-column">
        <div className="table-header">
            <div>{lang ? 'FECHA' : 'DATE'}</div>
            <div>{lang ? "CAMPAÑA" : 'CAMPAIGN'}</div>
            <div>{lang ? 'VER' : 'VIEW'}</div>
            <div>{lang ? 'COMPORTAMIENTO' : 'ACTIONS'}</div>
        </div>
        {data && data.map((listItem, index) => {
            const diff = findDateDiff(listItem.createdOn, lang);
            const isPositive = diff.days > 0;
            const isNegative = diff.days < 0;
            const isZero = diff.days === 0;
            if (!((isPositive && activeTab === 0) || (isZero && activeTab === 1) || (isNegative && activeTab === 2))) return null;
            return <CampaignTableRow
                lang={lang}
                key={index}
                diffText={diff.text}
                onChange={(date) => onChange(date, index)} data={listItem} openModal={() => {
                    setModalState(true);
                    setSelectedCampaign(index);
                }} 
                mode={mode}
                />
        })}
        {isModalOpen && <Modal lang={lang} close={() => setModalState(false)} modalData={data[selectedCampaign]} />}
    </div>
}

export default CampaignTable;

const findDateDiff = (createdDate, lang) => {
    const date1 = new Date();
    const date2 = new Date(createdDate);
    const diffTime = date2 - date1;
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

    const textEN = diffDays === 0 ? "Live" : diffDays < 0 ? `${Math.abs(diffDays)} day${Math.abs(diffDays) === 1 ? "" : "s"} ago` : `${Math.abs(diffDays)} day${Math.abs(diffDays) === 1 ? "" : "s"} to go`
    const textES = diffDays === 0 ? "En Vivo" : diffDays < 0 ? ` Hace ${Math.abs(diffDays)} día${Math.abs(diffDays) === 1 ? "" : "s"} ${Math.abs(diffDays)}` : `${Math.abs(diffDays)} día${Math.abs(diffDays) === 1 ? "" : "s"} mas`

    return { days: diffDays, text: lang ? textES : textEN };
}